package at.badkey.factorypattern.main;

import at.badkey.factorypattern.garment.Garment;
import at.badkey.factorypattern.garment.GarmentFactory;
import at.badkey.factorypattern.garment.GarmentObject;

public class Main {

	public static void main(String[] args) {
		GarmentFactory f = new GarmentFactory();
		
		GarmentObject o = f.createGarment(Garment.SHIRT);
		o.print();
		
		GarmentObject o1 = f.createGarment(Garment.TROUSERS);
		o1.print();

	}

}
