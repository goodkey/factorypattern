package at.badkey.factorypattern.garment;

public enum Garment {
	
	SHIRT,
	TROUSERS
	
}
