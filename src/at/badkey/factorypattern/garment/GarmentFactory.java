package at.badkey.factorypattern.garment;

public class GarmentFactory {
	
	public GarmentObject createGarment(Garment g) {
		if(g == Garment.SHIRT) {
			GarmentObject o = new Shirt();
			return o;
		}else if(g == Garment.TROUSERS) {
			GarmentObject o = new Trouser();
			return o;
		}
		throw new IllegalArgumentException("Unknown garment type");
	}

}
