package at.badkey.factorypattern.garment;

public class Shirt implements GarmentObject{
	
	public Shirt() {
		System.out.println("Created a shirt");
	}

	public void print() {
		System.out.println("I am a shirt");
	}
	
}
