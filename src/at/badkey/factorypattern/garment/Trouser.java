package at.badkey.factorypattern.garment;

public class Trouser implements GarmentObject{

	public Trouser() {
		System.out.println("Created a trouser");
	}

	public void print() {
		System.out.println("I am a trouser");
	}
	
	
}
