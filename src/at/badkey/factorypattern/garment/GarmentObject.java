package at.badkey.factorypattern.garment;

public interface GarmentObject {
	
	public void print();

}
